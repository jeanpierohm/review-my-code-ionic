angular.module('app.services', ['ngResource'])

.factory('Auth', [function(){
  var o = {};

  o.buildBasicAuth = function(username, password) {
    return window.btoa(username + ":" + password);
  };

  o.login = function (username, password) {
    window.localStorage.setItem("username", username);
    window.localStorage.setItem("password", password);
    window.localStorage.setItem("basicAuth", o.buildBasicAuth(username, password));
  };

  o.logout = function () {
    window.localStorage.setItem("basicAuth", undefined);
    window.localStorage.setItem("username", undefined);
    window.localStorage.setItem("password", undefined);
  };

  o.isAuthenticated = function () {
    return !["undefined", undefined, null, "null"].includes(window.localStorage.getItem("basicAuth"));
  };

  o.getBasicAuth = function () {
    return window.localStorage.getItem("basicAuth");
  };

  return o;
}])
