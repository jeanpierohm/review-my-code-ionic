function prettyError(object) {
  var message = '';
  for (var property in object)
    message += property + ': ' + object[property] + ' ';
  return message;
}

angular.module('app.controllers', [])

.controller('menuCtrl', ['$scope', '$stateParams', 'Auth', '$state', '$window',
function ($scope, $stateParams, Auth, $state, $window) {

  $scope.isAuthenticated = function () {
    return Auth.isAuthenticated();
  };

  $scope.logout = function () {
    Auth.logout();
    $window.location.reload(true);
  };

}])


.controller('reviewCtrl', ['$scope', '$stateParams', '$http', 'codesForReviewUrl',
            'ionicToast', 'Auth', '$ionicLoading',
function ($scope, $stateParams, $http, codesForReviewUrl, ionicToast,
          Auth, $ionicLoading) {

  var codes_list = [];
  $scope.data = {};
  fetchCodesForReview();

  function fetchCodesForReview() {
    $ionicLoading.show({template: '<ion-spinner></ion-spinner>'});
    $http.get(codesForReviewUrl, {headers: {"Authorization": "Basic " + Auth.getBasicAuth() }})
    .then(
      function (response) {
        $ionicLoading.hide();
        codes_list = response.data;
        $scope.data.code = codes_list.shift();
      },
      function (error) {
        if (error.status == 403) {
          Auth.logout();
        }
        else {
          ionicToast.show(': ' + JSON.stringify(error.data, null, 4), 'middle', false, 2500);
        }
      }
    );
  }

  $scope.publishReview = function(code, review){
    $ionicLoading.show({template: '<ion-spinner></ion-spinner>'});
    $http.post(
      reviewUrl,
      {comment: review, code: code.id, author: code.author.id},
      {headers: {"Authorization": "Basic " + Auth.getBasicAuth() }})
    .then(
      function (response) {
        $ionicLoading.hide();
        $scope.data.code = codes_list.shift();
        $scope.data.review = '';
        if ($scope.data.code == undefined) fetchCodesForReview();
        ionicToast.show('Review sent.', 'middle', false, 2500);
      },
      function (error) {
        $ionicLoading.hide();
        ionicToast.show('Error: ' + JSON.stringify(error.data, null, 4), 'middle', false, 2500);
      }
    );
  };

  $scope.skipReview = function () {
    $scope.data.code = codes_list.shift();
    if ($scope.data.code == undefined) fetchCodesForReview();
  }

}])


.controller('uploadYourCodeCtrl', ['$scope', '$stateParams', 'codeUrl', '$http',
            'ionicToast', 'Auth', '$ionicLoading',
function ($scope, $stateParams, codeUrl, $http, ionicToast, Auth,
          $ionicLoading) {

  $scope.data = {};

  $scope.publishCode = function(title, body) {
    $ionicLoading.show({template: '<ion-spinner></ion-spinner>'});
    $http.post(
      codeUrl,
      {title: title, body: body},
      {headers: {"Authorization": "Basic " + Auth.getBasicAuth() }}
    ).then(
      function (response) {
        $ionicLoading.hide();
        $scope.data.codeBody = '';
        $scope.data.codeTitle = '';
        ionicToast.show('Code snippet sent.', 'middle', false, 2500);
      },
      function (error) {
        $ionicLoading.hide();
        ionicToast.show('Error: ' + prettyError(error.data), 'middle', false, 2500);
      }
    );
  };

}])


.controller('myCodesCtrl', ['$scope', '$stateParams', 'Auth', '$http', '$ionicLoading',
function ($scope, $stateParams, Auth, $http, $ionicLoading) {
  $scope.data = {};
  fetchLatestReviewedCodes();

  function fetchLatestReviewedCodes() {
    $ionicLoading.show({template: '<ion-spinner></ion-spinner>'});
    $http.get(myCodesUrl, {headers: {"Authorization": "Basic " + Auth.getBasicAuth() }})
    .then(
      function (response) {
        $ionicLoading.hide();
        $scope.data.codes_list = response.data;
      },
      function (error) {
        $ionicLoading.hide();
        if (error.status == 403) {
          Auth.logout();
        }
        else {
          ionicToast.show('Error: ' + prettyError(error.data), 'middle', false, 2500);
        }
      }
    );
  }

  /*
   * if given group is the selected group, deselect it
   * else, select the given group
   */
  $scope.toggleCode = function(code) {
    if ($scope.isCodeShown(code)) {
      $scope.data.shownCode = null;
    } else {
      $scope.data.shownCode = code;
    }
  };

  $scope.isCodeShown = function(code) {
    return $scope.data.shownCode === code;
  };

}])


.controller('loginCtrl', ['$scope', '$stateParams', '$http', '$ionicLoading',
            'apiUrl', 'ionicToast', 'Auth', '$state',
function ($scope, $stateParams, $http, $ionicLoading, apiUrl,
          ionicToast, Auth, $state) {

  $scope.login = function (username, password) {
    $ionicLoading.show({template: '<ion-spinner></ion-spinner>'});

    var headers = {"Authorization": "Basic " + Auth.buildBasicAuth(username, password) };

    $http.get(apiUrl + 'core/api/check_credentials', {headers: headers})
    .then(
      function (response) {
        Auth.login(username, password);
        $ionicLoading.hide();
        $state.go('tabsController.uploadYourCode');
      },
      function (error) {
        $ionicLoading.hide();
        ionicToast.show('Error: ' + error.data.detail, 'middle', false, 2500);
      }
    )
  };

}])


.controller('signupCtrl', ['$scope', '$stateParams', '$http', '$ionicLoading',
            'apiUrl', 'ionicToast', 'Auth', '$state',
function ($scope, $stateParams, $http, $ionicLoading, apiUrl,
          ionicToast, Auth, $state) {

  $scope.signup = function (username, email, password) {
    $ionicLoading.show({template: '<ion-spinner></ion-spinner>'});

    $http.post(
      apiUrl + 'core/api/signup',
      {username: username, email: email, password: password}
    ).then(
      function (response) {
        Auth.login(username, password);
        $ionicLoading.hide();
        $state.go('tabsController.uploadYourCode');
      },
      function (error) {
        $ionicLoading.hide();
        ionicToast.show('Error: ' + error.data.detail, 'middle', false, 2500);
      }
    )
  };

}])


.controller('aboutCtrl', ['$scope', '$stateParams',
function ($scope, $stateParams) {


}])
