angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  .state('tabsController.review', {
    url: '/review',
    views: {
      'reviewTab': {
        templateUrl: 'templates/review.html',
        controller: 'reviewCtrl'
      }
    }
  })

  .state('tabsController.uploadYourCode', {
    url: '/upload',
    views: {
      'uploadCodeTab': {
        templateUrl: 'templates/uploadYourCode.html',
        controller: 'uploadYourCodeCtrl'
      }
    }
  })

  .state('tabsController.myCodes', {
    url: '/codes',
    views: {
      'myCodesTab': {
        templateUrl: 'templates/myCodes.html',
        controller: 'myCodesCtrl'
      }
    }
  })

  .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl',
    onEnter: function ($state, Auth) {
      if (Auth.isAuthenticated()) {
        $state.go('tabsController.uploadYourCode');
      }
    }
  })

  .state('signup', {
    url: '/signup',
    templateUrl: 'templates/signup.html',
    controller: 'signupCtrl'
  })

  .state('tabsController', {
    url: '/tabs',
    templateUrl: 'templates/tabsController.html',
    abstract:true,
    onEnter: function($state, Auth) {
      if (!Auth.isAuthenticated()) {
        $state.go('login');
      }
    }
  })

  .state('about', {
    url: '/about',
    templateUrl: 'templates/about.html',
    controller: 'aboutCtrl'
  })

$urlRouterProvider.otherwise('about')



});
